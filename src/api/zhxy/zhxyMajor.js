import request from '@/utils/request'

export function getAllMajor() {
  const params = {
    page: 0,
    size: 9999,
    enabled: true
  }
  return request({
    url: 'api/zhxyMajor',
    method: 'get',
    params
  })
}
export function getMajors(params) {
  return request({
    url: 'api/zhxyMajor',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'api/zhxyMajor',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/zhxyMajor/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/zhxyMajor',
    method: 'put',
    data
  })
}

export default { add, edit, del }
